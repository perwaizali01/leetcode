package com.example.Leetcode.LinkedList;

import java.util.LinkedList;

public class LinkedListDemo {

	public static void main(String[] args) {

		LinkedList<Integer> list = new LinkedList<>();
		list.add(100);
		list.add(2);
		list.add(11);
		list.add(70);
		list.add(310);
		
		System.out.println(list);
		
		list.addLast(600);
		list.addFirst(21);
		list.add(2, 51);
		System.out.println("--------------------------");
		System.out.println(list);
	}
}
