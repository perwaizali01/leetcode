package com.example.LeetCode_Codebix;

import java.util.ArrayList;
import java.util.List;

public class Pascal {

	public static List<List<Integer>> generate(int rowNumber) {

		List<List<Integer>> triangle = new ArrayList<>();

		if (rowNumber == 0)
			return triangle;

		List<Integer> first_row = new ArrayList<>();
		first_row.add(1);
		triangle.add(first_row);

		for (int i = 1; i < rowNumber; i++) {

			List<Integer> prev_row = triangle.get(i - 1);
			List<Integer> row = new ArrayList<>();

			row.add(1);

			for (int j = 1; j <i; j++) {

				row.add(prev_row.get(i - 1) + prev_row.get(j));
			}

			row.add(1);
			triangle.add(row);
		}
		System.out.println(triangle);
		return triangle;

	}

	public static void main(String[] args) {

		generate(5);

	}

}
