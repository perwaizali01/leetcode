package com.example.LeetCode_Codebix;

public class FindColumn {

	public static void main(String[] args) {
		
		int[] arr = {1,34,13,56,2,10,5,5,5};
		
		FindColumn fc = new FindColumn();
		
		System.out.println(fc.arraySunm(arr));

	}

	public int titleToNumber(String s) {
		
		int ans =0;
		int p =0;
		
		for(int i=s.length()-1; i>=0; i--) {
			char c = s.charAt(i);
			
			int val = (int)c -65+1;
			ans += val*Math.pow(26, p);
			p++;
		}
		
		
		return p;
	}
	
	public int arraySunm(int[] arr) {
		
		int number =15;
		
		int apperedNum =0;
		
		for(int i=0; i<arr.length; i++) {
			for(int j=i+1;j<arr.length-1; j++) {
				
				if(arr[i]+ arr[j]==15) {
					apperedNum++;
				}
			}
		}
		
		return apperedNum;
	}

}

