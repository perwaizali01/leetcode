package com.example.LeetCode_Codebix;

import java.util.HashSet;
import java.util.Set;

public class checkDuplicateInArray {

	public static boolean duplicatCheck(int[] nums) {

		Set<Integer> set = new HashSet<>();

		for (int i = 0; i < nums.length; i++) {
			if (set.contains(nums[i])) {
				return true;
			}
			set.add(nums[i]);
		}
		return false;
	}

	public static void main(String[] args) {

		int[] arr = {1,4,3,67};
		boolean result = duplicatCheck(arr);
		System.out.println(result);
		
		
	}

}
