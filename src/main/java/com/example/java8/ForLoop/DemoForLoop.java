package com.example.java8.ForLoop;

import java.util.Arrays;
import java.util.List;

public class DemoForLoop {

	public static void main(String[] args) {
	
		List<Integer> list = Arrays.asList(12,34,56,1);
		
		/*
		 * for(int i=0; i<list.size();i++) { System.out.println(list.get(i)); }
		 */
		
		list.forEach(x->System.out.println(x));
		
	}
	
	
}
