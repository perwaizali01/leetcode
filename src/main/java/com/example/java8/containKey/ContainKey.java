package com.example.java8.containKey;

import java.util.HashMap;
import java.util.Map;

public class ContainKey {

	public static void main(String[] args) {

		Map <String, String> mapOfKey = new HashMap<>();
		
		mapOfKey.put("123", "UberEats-1");
		mapOfKey.put("456", "UberEats-2");
		mapOfKey.put("789", "UberEats-3");
		
			
		
		if(mapOfKey.containsValue("UberEats-34")) {
			//System.out.println("yes");
		}else {
			//System.out.println("No");
		}
		
		
		Map <String, String> mapOfKey1 = Map.of(
				"ABC","123",
				"DEF","456",
				"GHI","789"
				);
		
		if(mapOfKey1.containsKey("ABC")) {
			System.out.println("yes");
	}
	}
}
