package com.example.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EmployeeSortedByDep {

	public static void main(String[] args) {

		List<Employee> listOfEmp = new ArrayList<>();
		
		listOfEmp.add(new Employee(100,"Ali_IT_1",1,100.00,"active"));
		listOfEmp.add(new Employee(101,"Ali_HR_1",2,200.00,"active"));
		listOfEmp.add(new Employee(103,"Ali_Manager_1",3,250.00,"Inactive"));
		listOfEmp.add(new Employee(104,"Ali_IT_2",1,300.00,"active"));
		listOfEmp.add(new Employee(105,"Ali_HR_2",2,350.00,"active"));
		
		
		Map<Integer, List<Employee>> listFinal = listOfEmp.stream()
				                                          .collect(Collectors.groupingBy(Employee::getDeptId,Collectors.toList()));
		listFinal.entrySet().forEach(entry -> {
			System.out.println(entry.getKey()+ " ------ " + entry.getValue());
		
		});
		
		Map<Integer, Long> emplCountByDep = listOfEmp.stream().collect(Collectors.groupingBy(Employee::getDeptId, Collectors.counting()));
		emplCountByDep.entrySet().forEach(entry ->{
			System.out.println(entry.getKey() + "------" + entry.getValue());
		});
		
		long activeUser = listOfEmp.stream().filter(e -> "active".equals(e.getStatus())).count();
		System.out.println("Number of Active Users : "+ activeUser);
		
		
		Map<String, List<Employee>> emplCountByName  = listOfEmp.stream().filter(e -> "active".equals(e.getStatus()))
		.collect(Collectors.groupingBy(Employee::getName,Collectors.toList())); 

		emplCountByName.entrySet().forEach(entry ->{
			System.out.println(entry.getKey() + "------" + entry.getValue());
		});
		
	}

}
