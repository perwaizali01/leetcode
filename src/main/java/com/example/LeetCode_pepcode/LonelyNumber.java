package com.example.LeetCode_pepcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LonelyNumber {

	public static void main(String[] args) {

		int[] array = { 10, 6, 5, 8 };

		HashMap<Integer, Integer> map = new HashMap<>();
		List<Integer> ans = new ArrayList<>();

		for (int ele : array) {
			map.put(ele, map.getOrDefault(ele ,0)+ 1);
		}

		for (int ele : array) {
			if (map.get(ele) > 1 || map.containsKey(ele - 1) || map.containsKey(ele + 1))
				continue;
			ans.add(ele);
		}
		System.out.println(map);
	}

}
