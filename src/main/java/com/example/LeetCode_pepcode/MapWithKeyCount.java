package com.example.LeetCode_pepcode;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class MapWithKeyCount {

	public static void main(String[] args) {

		List<Integer> list = Arrays.asList(12, 1, 23, 45, 6, 12, 12, 1, 23);
		Map<Integer, Integer> map = new HashMap<>();
		TreeSet<Integer> duplicateEle = new TreeSet<>();
		

		for (int ele : list) {

			map.put(ele, map.getOrDefault(ele, 0) + 1);

		}
		for (int ele : list) {
			if (map.get(ele) > 1) {
				duplicateEle.add(ele);
			}
		}
		TreeSet reverseTreeSet = (TreeSet) duplicateEle.descendingSet();
		System.out.println(map);
		System.out.println("----------------------------");
		System.out.println(duplicateEle);
		System.out.println("----------------------------");
		System.out.println(reverseTreeSet);
	}

}
